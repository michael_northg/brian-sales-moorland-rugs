﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Configuration;

namespace Brian_Sales.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CompanyHistory()
        {
            return View();
        }

        public ActionResult Development()
        {
            return View();
        }

        public ActionResult RugDesigns()
        {
            return View();
        }

        public ActionResult Enquiries()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Enquiry(string name, string telephone, string emailAddress, string message)
        {
            MailMessage emailMessage = new MailMessage();
            emailMessage.To.Add(ConfigurationManager.AppSettings["ToAddress"]); //recipient
            emailMessage.Subject = "This is the Subject line";
            emailMessage.From = new MailAddress(ConfigurationManager.AppSettings["FromAddress"]); //from email
            emailMessage.Body = "Name: " + name + "<br/>Telephone: " + telephone;
            SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SmtpClient"]);  // you need an smtp server address to send emails
            smtp.Send(emailMessage);

            return RedirectToAction("EnquiryThanks");
        }

        public ActionResult EnquiryThanks()
        {
            return View();
        }

        public ActionResult OrderSamples()
        {
            return View();
        }

        public ActionResult Prices()
        {
            return View();
        }

        public ActionResult News()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }

        public ActionResult FAQs()
        {
            return View();
        }
    }
}
